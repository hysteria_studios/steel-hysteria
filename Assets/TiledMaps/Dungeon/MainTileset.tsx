<?xml version="1.0" encoding="UTF-8"?>
<tileset name="MainTileset" tilewidth="16" tileheight="16" tilecount="288" columns="16">
 <image source="dungeon-prison-theme-tilesheet.png" width="256" height="288"/>
 <tile id="287">
  <objectgroup draworder="index">
   <object id="1" x="-0.0434783" y="-0.0434783" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
