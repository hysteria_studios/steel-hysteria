<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Steampunk Blocks" tilewidth="16" tileheight="16" tilecount="96" columns="12">
 <image source="Steampunk Blocks.png" width="192" height="128"/>
 <tile id="0">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="26">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="74">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
