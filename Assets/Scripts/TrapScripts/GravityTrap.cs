﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityTrap : MonoBehaviour {

    //[Header("Vector3")]
    private Vector2 originGravity;
    private Vector2 newGravity;

    //[Header("Boolean")]
    private bool isPossessed;
    private bool desposses;
    private bool recentPossession;
    private bool trapActivation;
    private bool activationController;

    //[Header("Transform")]
    private Transform _transform;

    //[Header("Image")]
    private Sprite[] gravityButton;

    //[Header("GameManager")]
    private GameManager gameManager;

    //[Header("GameObject")]
    public Character2D owner;

	// Use this for initialization
	void Start () {
        SettingReferences();
	}
	
	// Update is called once per frame
	void Update () {
        if (!gameManager.isStarting && !gameManager.roundFinished)
        {
            if (isPossessed)
            {
                trapActivation = InputManager.Instance.TrapFireButton(owner.player);
                desposses = InputManager.Instance.PossesButton(owner.player);

                if (recentPossession)
                {
                    StartCoroutine(LetDesposses());
                }

                if (desposses && !recentPossession)
                {
                    LeavePossession();
                }

                if (trapActivation)
                {
                    if (!activationController)
                    {
                        _transform.GetComponent<SpriteRenderer>().sprite = gravityButton[2];
                        Physics2D.gravity = newGravity;
                        activationController = true;
                    }
                    else
                    {
                        _transform.GetComponent<SpriteRenderer>().sprite = gravityButton[0];
                        Physics2D.gravity = originGravity;
                        activationController = false;
                    }
                }
            }
        }
    }

    IEnumerator LetDesposses()
    {
        yield return new WaitForSeconds(0.5f);
        recentPossession = false;
    }

    void LeavePossession()
    {
        owner.transform.position = _transform.position;
        owner.Desposses();
        owner = null;
        isPossessed = false;
    }

    public void GetPossesed(Transform player)
    {
        owner = player.GetComponent<Character2D>();
        isPossessed = true;
        recentPossession = true;
    }

    void SettingReferences()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        isPossessed = false;
        desposses = false;
        recentPossession = false;
        activationController = false;
        gravityButton = Resources.LoadAll<Sprite>("Sprites/GravityButton");
        originGravity = Physics2D.gravity;
        newGravity = new Vector2(0, -4.8f);
        _transform = GetComponent<Transform>();
        owner = null;
    }
}
