﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTrap : MonoBehaviour {

    //[Header("Float")]
    private float delayTime;
    private float animDuration;

    //[Header("Boolean")]
    private bool isPossessed;
    private bool canShot;
    private bool desposses;
    private bool fireBttn;
    private bool recentPossession;

    //[Header("Transform")]
    private Transform _laserObj;

    //[Header("Managers")]
    private GameManager gameManager;
    private SoundManager soundManager;

    //[Header("GameObject")]
    public Character2D owner;

    // Use this for initialization
    void Start ()
    {
        SettingReferences();
        StartCoroutine(LaserCoolDown());
	}

    // Update is called once per frame
    void Update()
    {
        if (!gameManager.isStarting && !gameManager.roundFinished) {
            if (!isPossessed)
            {
                if (canShot)
                {
                    _laserObj.gameObject.SetActive(true);
                    canShot = false;
                    StartCoroutine(LaserActive());
                }
            }
            else
            {
                fireBttn = InputManager.Instance.TrapFireButton(owner.player);
                desposses = InputManager.Instance.PossesButton(owner.player);

                if (recentPossession)
                {
                    StartCoroutine(LetDesposses());
                }

                if (desposses && !recentPossession)
                {
                    LeavePossession();
                }

                if (fireBttn && canShot)
                {
                    _laserObj.gameObject.SetActive(true);
                    canShot = false;
                    StartCoroutine(LaserActive());
                }
            }
        }
    }

    IEnumerator LetDesposses()
    {
        yield return new WaitForSeconds(0.5f);
        recentPossession = false;
    }

    IEnumerator LaserActive()
    {
        soundManager.GetSoundEffectsSource().PlayOneShot(soundManager.GetSoundEffects("Laser"), 0.5f);
        yield return new WaitForSeconds(animDuration);
        _laserObj.gameObject.SetActive(false);
        StartCoroutine(LaserCoolDown());
    }

    IEnumerator LaserCoolDown()
    {
        yield return new WaitForSeconds(delayTime);
        canShot = true;
    }

    void LeavePossession()
    {
        owner.transform.position = _laserObj.position;
        owner.Desposses();
        owner = null;
        isPossessed = false;
        delayTime = 3f;
    }

    public void GetPossesed(Transform player)
    {
        owner = player.GetComponent<Character2D>();
        canShot = true;
        isPossessed = true;
        recentPossession = true;
        delayTime = 2f;
    }

    void SettingReferences()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        owner = null;
        delayTime = 3f;
        animDuration = 1f;
        isPossessed = false;
        canShot = false;
        fireBttn = false;
        recentPossession = false;
        _laserObj = transform.Find("Animation");
    }
}
