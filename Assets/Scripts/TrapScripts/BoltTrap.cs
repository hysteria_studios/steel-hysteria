﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltTrap : MonoBehaviour {

    //[Header("Float")]
    private float delayTime;
    private float animDuration;

    //[Header("Boolean")]
    private bool isPossessed;
    private bool canShot;
    private bool desposses;
    private bool fireBttn;
    private bool recentPossession;

    //[Header("Managers")]
    private GameManager gameManager;
    private SoundManager soundManager;

    //[Header("Transform")]
    private Transform _boltObj;

    //[Header("GameObject")]
    public Character2D owner;

    // Use this for initialization
    void Start ()
    {
        SettingReferences();
        StartCoroutine(ShotCoolDown());
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!gameManager.isStarting && !gameManager.roundFinished)
        {
            if (!isPossessed)
            {
                if (canShot)
                {
                    _boltObj.gameObject.SetActive(true);
                    canShot = false;
                    StartCoroutine(BoltShot());
                }
            }
            else
            {
                fireBttn = InputManager.Instance.TrapFireButton(owner.player);
                desposses = InputManager.Instance.PossesButton(owner.player);

                if (recentPossession)
                {
                    StartCoroutine(LetDesposses());
                }

                if (desposses && !recentPossession)
                {
                    LeavePossession();
                }

                if (fireBttn && canShot)
                {
                    _boltObj.gameObject.SetActive(true);
                    canShot = false;
                    StartCoroutine(BoltShot());
                }
            }
        }
	}

    IEnumerator LetDesposses()
    {
        yield return new WaitForSeconds(0.5f);
        recentPossession = false;
    }

    IEnumerator BoltShot()
    {
        soundManager.GetSoundEffectsSource().PlayOneShot(soundManager.GetSoundEffects("Bolt"), 0.5f);
        yield return new WaitForSeconds(animDuration);
        _boltObj.gameObject.SetActive(false);
        StartCoroutine(ShotCoolDown());
    }

    IEnumerator ShotCoolDown()
    {
        yield return new WaitForSeconds(delayTime);
        canShot = true;
    }

    void LeavePossession()
    {
        owner.transform.position = _boltObj.position;
        owner.Desposses();
        owner = null;
        isPossessed = false;
        delayTime = 2f;
    }

    public void GetPossesed(Transform player)
    {
        owner = player.GetComponent<Character2D>();
        canShot = true;
        isPossessed = true;
        recentPossession = true;
        delayTime = 1f;
    }

    void SettingReferences()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        owner = null;
        delayTime = 2f;
        animDuration = 0.45f;
        isPossessed = false;
        canShot = false;
        fireBttn = false;
        recentPossession = false;
        _boltObj = transform.Find("Bolt");
    }
}
