﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawTrap : MonoBehaviour {

    //[Header("Float")]
    private float speed;
    private float rotateSpd;
    private float hMov;
    private float maxPosX;

    //[Header("Boolean")]
    private bool isPossessed;
    private bool isGoingRight;
    private bool desposses;
    private bool recentPossession;

    //[Header("Transform")]
    private Transform _transform;
    private Transform _transformSprite;
    private Transform aPoint;
    private Transform bPoint;

    //[Header("Vector3")]
    private Vector3 tmpPosition;

    //[Header("GameManager")]
    private GameManager gameManager;

    //[Header("GameObject")]
    public Character2D owner;

	// Use this for initialization
	void Start () {
        SettingReferences();
	}
	
	// Update is called once per frame
	void Update () {

        if (!gameManager.isStarting && !gameManager.roundFinished)
        {
            if (!isPossessed)
            {
                if (isGoingRight)
                {

                    if (transform.position.x > bPoint.position.x)
                    {
                        SwitchDirection();
                    }

                    _transform.Translate(speed * Time.deltaTime, 0, 0);
                }
                else
                {
                    if (transform.position.x < aPoint.position.x)
                    {
                        SwitchDirection();
                    }

                    _transform.Translate(-speed * Time.deltaTime, 0, 0);
                }
                _transformSprite.Rotate(0, 0, rotateSpd * Time.deltaTime);
            }
            else
            {
                hMov = InputManager.Instance.GetAxisHorizontal(owner.player);
                desposses = InputManager.Instance.PossesButton(owner.player);

                if (recentPossession)
                {
                    StartCoroutine(LetDesposses());
                }

                if (desposses && !recentPossession)
                {
                    LeavePossession();
                }

                _transform.Translate(speed * hMov * Time.deltaTime, 0, 0);

                if (_transform.position.x > bPoint.position.x)
                {
                    tmpPosition = new Vector3(bPoint.position.x, transform.position.y, transform.position.z);
                    transform.position = tmpPosition;
                }

                if (_transform.position.x < aPoint.position.x)
                {
                    tmpPosition = new Vector3(aPoint.position.x, transform.position.y, transform.position.z);
                    transform.position = tmpPosition;
                }
                _transformSprite.Rotate(0, 0, rotateSpd * Time.deltaTime);
            }
        }
    }

    IEnumerator LetDesposses()
    {
        yield return new WaitForSeconds(0.5f);
        recentPossession = false;
    }

    void SwitchDirection()
    {
        isGoingRight = !isGoingRight;
    }

    void LeavePossession()
    {
        owner.transform.position = _transform.position;
        owner.Desposses();
        owner = null;
        isPossessed = false;
    }

    public void GetPossesed(Transform player)
    {
        owner = player.GetComponent<Character2D>();
        isPossessed = true;
        recentPossession = true;
    }

    void SettingReferences()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        aPoint = transform.parent.Find("PointA");
        bPoint = transform.parent.Find("PointB");
        _transform = GetComponent<Transform>();
        _transformSprite = _transform.Find("Sprite");
        speed = 5;
        rotateSpd = 10000;
        hMov = 0;
        tmpPosition = Vector3.zero;
        isGoingRight = true;
        isPossessed = false;
        desposses = false;
        recentPossession = false;
    }
}
