﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEngine :   MonoBehaviour {

    //[Header("Instance")]
    public static GameEngine instance;
    
    //[Header("Dictionary")]
    public Dictionary<InputManager.Player, int> scoreTable = new Dictionary<InputManager.Player, int>();
    
    //[Header("Integrer")]
    public int numPlayers = 2;
    public int currentPoints = 0;
    public int maxPoints = 5;

    //[Header("MenuManager")]
    public MenuManager menuManager;
    public MusicManager musicManager;

    //[Header("Others")]
    public Scrollbar scrollBar;
    public Toggle toggle;

    //[Header("Boolean")]
    public bool winner = false;
    public bool isFullscreen;

    //[Header("Player")]
    public InputManager.Player player;

    //[Header("String")]
    private string[] sceneNames = {"Factory", "Dungeon", "Desert", "LunarStation"};

    private void Awake()
    {
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }

    private void Start()
    {
        isFullscreen = Screen.fullScreen;
        toggle.isOn = !isFullscreen;
    }

    public void ChangeMap()
    {
        for (int i = 0; i < numPlayers; i++)
        {
            switch (i)
            {
                case (int)InputManager.Player.PLAYER_1:
                    if(scoreTable[InputManager.Player.PLAYER_1] > currentPoints)
                    {
                        currentPoints = scoreTable[InputManager.Player.PLAYER_1];
                        player = InputManager.Player.PLAYER_1;
                    }
                    break;
                case (int)InputManager.Player.PLAYER_2:
                    if(scoreTable[InputManager.Player.PLAYER_2] > currentPoints)
                    {
                        currentPoints = scoreTable[InputManager.Player.PLAYER_2];
                        player = InputManager.Player.PLAYER_2;
                    }
                    break;
                case (int)InputManager.Player.PLAYER_3:
                    if (scoreTable[InputManager.Player.PLAYER_3] > currentPoints)
                    {
                        currentPoints = scoreTable[InputManager.Player.PLAYER_3];
                        player = InputManager.Player.PLAYER_3;
                    }
                    break;
                case (int)InputManager.Player.PLAYER_4:
                    if (scoreTable[InputManager.Player.PLAYER_4] > currentPoints)
                    {
                        currentPoints = scoreTable[InputManager.Player.PLAYER_4];
                        player = InputManager.Player.PLAYER_4;
                    }
                    break;
                default:
                    break;
            }
            if (maxPoints == currentPoints)
            {
                winner = true;
                break;
            }
        }
        if (winner)
        {
            foreach (Transform child in menuManager.transform)
            {
                child.gameObject.SetActive(false);
            }
            foreach (AudioClip clip in musicManager.GetMusic())
            {
                if (clip.name == "MorningRoutine")
                {
                    musicManager.GetMusicSource().clip = clip;
                   break;
                }
            }
            musicManager.GetMusicSource().Play();
            foreach (Transform child in menuManager.transform)
            {
                if (child.name == "Win")
                {
                    child.gameObject.SetActive(true);
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            int random = Random.Range(0, sceneNames.Length);
            SceneManager.LoadScene(sceneNames[random]);
            int mRandom = Random.Range(0, musicManager.GetMusic().Length);
            if(musicManager.GetMusic()[mRandom].name == "MorningRoutine")
            {
                mRandom--;
            }
            musicManager.GetMusicSource().clip = musicManager.GetMusic()[mRandom];
            musicManager.GetMusicSource().Play();
        }
    }
    
    public void InitGame(int players)
    {
        numPlayers = players;
        for (int i = 0; i < numPlayers; i++)
        {
            switch (i)
            {
                case (int)InputManager.Player.PLAYER_1:
                    scoreTable.Add(InputManager.Player.PLAYER_1, 0);
                    break;
                case (int)InputManager.Player.PLAYER_2:
                    scoreTable.Add(InputManager.Player.PLAYER_2, 0);
                    break;
                case (int)InputManager.Player.PLAYER_3:
                    scoreTable.Add(InputManager.Player.PLAYER_3, 0);
                    break;
                case (int)InputManager.Player.PLAYER_4:
                    scoreTable.Add(InputManager.Player.PLAYER_4, 0);
                    break;
                default:
                    break;
            }
        }
        int random = Random.Range(0, sceneNames.Length);
        SceneManager.LoadScene(sceneNames[random]);
        int mRandom = Random.Range(0, musicManager.GetMusic().Length);
        musicManager.GetMusicSource().clip = musicManager.GetMusic()[mRandom];
        musicManager.GetMusicSource().Play();
    }

    public void FullScreen()
    {
        if (!isFullscreen)
        {
            Screen.fullScreen = true;
            isFullscreen = true;
        }
        else
        {
            Screen.fullScreen = false;
            isFullscreen = false;
        }
        ChangeResolution();
    }

    public void ChangeResolution()
    {
        if (!isFullscreen)
        {
            Screen.SetResolution(1920, 1080, true);
            for (int i = 0; i < menuManager.transform.childCount; i++)
            {
                if (menuManager.transform.GetChild(i).name != "Pause")
                {
                    menuManager.transform.GetChild(i).GetComponent<CanvasScaler>().scaleFactor = 1;
                }
            }
        }
        else
        {
            Screen.SetResolution(1280, 720, false);
            for (int i = 0; i < menuManager.transform.childCount; i++)
            {
                if (menuManager.transform.GetChild(i).name != "Pause")
                {
                    menuManager.transform.GetChild(i).GetComponent<CanvasScaler>().scaleFactor = 0.7f;
                }
            }
        }
    }

    public void Reset()
    {
        foreach (Transform child in menuManager.transform)
        {
            if (child.name != "MainMenu")
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }
        scoreTable.Clear();
        numPlayers = 2;
        currentPoints = 0;
        winner = false;
        musicManager.GetMusicSource().Stop();
    }
}