﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    //[Header("Transform")]
    private Transform spawnPointsContainer;
    private Transform playersContainer;
    private Transform playersInterface;
    public Transform scoreTable;

    //[Header("CanvasProperties")]
    private Canvas ui;
    private CountDown countDown;
    private Text winText;

    //[Header("GameObject")]
    private GameObject playerPref;

    //[Header("Integrer")]
    public int numPlayers = 2;
    public int playersAlive = 0;

    //[Header("Boolean")]
    public bool isStarting = true;
    public bool roundFinished = false;
    private bool continueButton = false;

    //[Header("Other")]
    private GameEngine gameEngine;
    private MenuManager menuManager;
    private Vector2 originalGravity;

    void Awake()
    {
        gameEngine = GameObject.Find("GameEngine").GetComponent<GameEngine>();
        menuManager = GameObject.Find("MenuManager").GetComponent<MenuManager>();
        foreach (Transform child in menuManager.transform)
        {
            child.gameObject.SetActive(true);
        }
        numPlayers = gameEngine.numPlayers;
        originalGravity = Physics2D.gravity;
        if (SceneManager.GetActiveScene().name == "LunarStation")
        {
            Physics2D.gravity.Set(0, -2.4f);
        }
        else
        {
            Physics2D.gravity.Set(originalGravity.x, originalGravity.y);
        }
        scoreTable = GameObject.Find("/MenuManager/ScoreTable").transform;
        ui = GameObject.Find("/MenuManager/Canvas").GetComponent<Canvas>();
        ui.gameObject.SetActive(true);
        countDown = ui.transform.Find("CountDown").gameObject.GetComponent<CountDown>();
        countDown.gameObject.SetActive(true);
        spawnPointsContainer = transform.Find("/SceneGameObjects/SpawnPoints");
        winText = GameObject.Find("/MenuManager/Canvas/WinMessage").GetComponent<Text>();
        winText.gameObject.SetActive(false);
        playersContainer = transform.Find("/SceneGameObjects/Players");
        playerPref = (GameObject)Resources.Load("Prefabs/Player_");
        playersInterface = GameObject.Find("/MenuManager/Interface").transform;
        foreach (Transform child in playersInterface)
        {
            child.gameObject.SetActive(false);
        }
        
    }

    // Use this for initialization
    void Start() {
        SpawnPlayers(numPlayers);
        StartCoroutine(StartGame());
        foreach (Transform child in menuManager.transform)
        {
            if (child.name != "Interface" && child.name != "Canvas" && child.name != "Pause")
            {
                child.gameObject.SetActive(false);
            }

        }
        for (int i = 0; i < numPlayers; i++)
        {
            playersInterface.GetChild(i).gameObject.SetActive(true);
        }
        playersInterface.Find("InterfaceCollision").gameObject.SetActive(true);
    }

    void Update()
    {
        if (scoreTable.gameObject.activeSelf && roundFinished)
        {
            continueButton = InputManager.Instance.ContinueButton();

            if (continueButton)
            {
                StartCoroutine(ChangeMap());
            }
        }
    }

    IEnumerator StartGame()
    {
        yield return StartCoroutine(countDown.StartCountDown(this));
        playersInterface.gameObject.SetActive(true);
        countDown.gameObject.SetActive(false);
        playersAlive = numPlayers;
    }

    public void PlayerDeath(InputManager.Player player)
    {
        playersAlive--;

        if (playersAlive == 0)
        {
            PlayerDraw();
        }
        else if (playersAlive == 1)
        {
            foreach (Transform child in playersContainer)
            {
                if (!child.GetComponent<Character2D>().deathMasterEnabled)
                {
                    player = child.GetComponent<Character2D>().player;
                }
            }
            StartCoroutine(LastSeconds(player));
        }
    }

    IEnumerator LastSeconds(InputManager.Player player)
    {
        yield return new WaitForSeconds(5);

        PlayerWin(player);
    }

    public void PlayerDraw()
    {
        ui.gameObject.SetActive(true);
        winText.text = "That's a draw!";
        StartCoroutine(TextAnimation());
    }

    public void PlayerWin(InputManager.Player player)
    {
        ui.gameObject.SetActive(true);
        winText.gameObject.SetActive(true);

        switch (player)
        {
            case InputManager.Player.PLAYER_1:
                winText.text = "Player 1 wins!";
                gameEngine.scoreTable[player] += 1;
                break;
            case InputManager.Player.PLAYER_2:
                winText.text = "Player 2 wins!";
                gameEngine.scoreTable[player] += 1;
                break;
            case InputManager.Player.PLAYER_3:
                winText.text = "Player 3 wins!";
                gameEngine.scoreTable[player] += 1;
                break;
            case InputManager.Player.PLAYER_4:
                winText.text = "Player 4 wins!";
                gameEngine.scoreTable[player] += 1;
                break;
            default:
                break;
        }
        FinishRound();
        StartCoroutine(TextAnimation());
    }

    IEnumerator TextAnimation()
    {
        for (int i = 0; i < 10; i++)
        {
            if (i % 2 == 0)
            {
                yield return new WaitForSeconds(0.25f);
                winText.gameObject.SetActive(false);
            }
            else
            {
                yield return new WaitForSeconds(0.25f);
                winText.gameObject.SetActive(true);
            }
        }

        ShowScoreTable();
    }

    private void ShowScoreTable()
    {
        scoreTable.gameObject.SetActive(true);
        foreach (Transform player in playersContainer)
        {
            player.GetComponent<Character2D>().ScoresTableText();
        }
    }

    IEnumerator ChangeMap()
    {
        yield return new WaitForSeconds(2);
        gameEngine.ChangeMap();
    }

    public void FinishRound()
    {
        roundFinished = true;
    }

    public void SpawnPlayers(int players) {
        ArrayList rangeAssigned = new ArrayList();
        int maxSpawnPoints = 4;
        Vector3 spawnPosition = Vector3.one;
        for (int i = 0; i < maxSpawnPoints; i++)
        {
            rangeAssigned.Add(-1);
        }
        int range = 0;
        bool validRange = false;
        for (int i = 0; i < players; i++)
        {
            for(int j = 0; j < rangeAssigned.Count; j++)
            {
                range = Random.Range(0, spawnPointsContainer.childCount);
                if (!rangeAssigned.Contains(range)) {
                    rangeAssigned[i] = range;
                    validRange = true;
                    break;
                }
            }
            if(validRange)
            {
                spawnPosition = spawnPointsContainer.GetChild(range).position;
            }

            playerPref.name = "Player_" + (i + 1);
            Instantiate(playerPref, spawnPosition, new Quaternion(0, 0, 0, 0), playersContainer);
        }
        scoreTable.gameObject.SetActive(false);

    }
}
