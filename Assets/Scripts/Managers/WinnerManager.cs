﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinnerManager : MonoBehaviour {

    public Text congratsText;

    private bool continueButton = false;
    private bool textEnable = true;
    private bool canChange = true;

    private GameEngine gameEngine;

	// Use this for initialization
	void Start () {
        gameEngine = GameObject.Find("GameEngine").GetComponent<GameEngine>();

        if (gameEngine.player == InputManager.Player.PLAYER_1)
        {
            congratsText.text = "Congratulations Player 1";
            transform.GetComponent<Image>().color = new Color(0.7f, 0, 0, 1);
        }
        else if(gameEngine.player == InputManager.Player.PLAYER_2)
        {
            congratsText.text = "Congratulations Player 2";
            transform.GetComponent<Image>().color = new Color(0, 0, 0.7f, 1);
        }
        else if (gameEngine.player == InputManager.Player.PLAYER_3)
        {
            congratsText.text = "Congratulations Player 3";
            transform.GetComponent<Image>().color = new Color(0, 0.7f, 0, 1);
        }
        else if (gameEngine.player == InputManager.Player.PLAYER_4)
        {
            congratsText.text = "Congratulations Player 4";
            transform.GetComponent<Image>().color = new Color(0.56f, 0.45f, 0.036f, 1);
        }
    }
	
	// Update is called once per frame
	void Update () {
        continueButton = InputManager.Instance.ContinueButton();
        if (continueButton)
        {
            StartCoroutine(ToMainMenu());
        }

        if (textEnable && canChange)
        {
            congratsText.gameObject.SetActive(true);
            canChange = false;
            StartCoroutine(TextAnimation());
        }
        else if(!textEnable && canChange)
        {
            congratsText.gameObject.SetActive(false);
            canChange = false;
            StartCoroutine(TextAnimation());
        }
	}

    IEnumerator TextAnimation()
    {
        yield return new WaitForSeconds(0.5f);
        textEnable = !textEnable;
        canChange = true;
    }

    IEnumerator ToMainMenu()
    {
        yield return new WaitForSeconds(1);
        gameEngine.Reset();
        SceneManager.LoadScene("Menu");
    }
}
