﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

    //[Header("Instance")]
    public static SoundManager instance;

    //[Header("Sounds")]
    public AudioClip[] effects;
    private AudioSource source;

    //[Header("Other")]
    public Scrollbar scrollbar;

    private void Awake()
    {
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }

    // Use this for initialization
    void Start()
    {
        source = transform.GetComponent<AudioSource>();
        scrollbar.value = source.volume;
        effects = Resources.LoadAll<AudioClip>("Sounds/SoundEffects");
    }

    public AudioClip GetSoundEffects(string soundName)
    {
        AudioClip sound = null;
        foreach (AudioClip clip in effects)
        {
            if (clip.name == soundName)
            {
                sound = clip;
                break;
            }
        }
        return sound;
    }

    public AudioSource GetSoundEffectsSource()
    {
        return source;
    }

    public Scrollbar GetSoundEffectScrollbar()
    {
        return scrollbar;
    }

    public void ChangeSoundEffectsVolume()
    {
        source.volume = scrollbar.value;
        source.PlayOneShot(GetSoundEffects("Shotgun"), 0.5f);
    }
}
