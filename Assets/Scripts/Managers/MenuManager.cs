﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    //[Header("Instance")]
    public static MenuManager instance;

    //[Header("Transform")]
    public Transform optionMenu;
    public Transform playersChoose;
    public Transform mainMenu;
    public Transform audioSubMenu;
    public Transform videoSubMenu;
    public Transform pauseMenu;

    //[Header("Boolean")]
    private bool backButton = false;

    //[Header("Other References")]
    private GameEngine gameEngine;

    private void Awake()
    {
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        gameEngine = GameObject.Find("GameEngine").GetComponent<GameEngine>();
    }

    private void Start()
    {
        foreach (Transform child in transform)
        {
            if (child.name == "OptionsMenu")
            {
                child.Find("Menu/ButtonVideo/VideoContent/FullScreen/Toggle").GetComponent<Toggle>().isOn = gameEngine.isFullscreen;
                child.Find("Menu/ButtonVideo/VideoContent").gameObject.SetActive(false);
            }
        }
        foreach (Transform child in transform)
        {
            if (child.gameObject.name != "MainMenu")
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    private void Update()
    {
        backButton = InputManager.Instance.BackButton();

        if (backButton)
        {
            BackButton();
        }
    }

    public void StartButton()
    {
        playersChoose.gameObject.SetActive(true);
        mainMenu.gameObject.SetActive(false);
    }

    public void OptoinsButton()
    {
        optionMenu.gameObject.SetActive(true);
        mainMenu.gameObject.SetActive(false);
    }

    public void VideoSettingsButton()
    {
        videoSubMenu.gameObject.SetActive(true);
        audioSubMenu.gameObject.SetActive(false);
    }

    public void MusicSettingsButton()
    {
        audioSubMenu.gameObject.SetActive(true);
        videoSubMenu.gameObject.SetActive(false);
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    public void BackButton()
    {
        if (optionMenu.gameObject.activeSelf && SceneManager.GetActiveScene().name == "Menu")
        {
            mainMenu.gameObject.SetActive(true);
            optionMenu.gameObject.SetActive(false);
        }
        else if (playersChoose.gameObject.activeSelf && SceneManager.GetActiveScene().name == "Menu")
        {
            mainMenu.gameObject.SetActive(true);
            playersChoose.gameObject.SetActive(false);
        }
        else if (optionMenu.gameObject.activeSelf)
        {
            pauseMenu.gameObject.SetActive(true);
            pauseMenu.GetChild(0).gameObject.SetActive(true);
            optionMenu.gameObject.SetActive(false);
        }
    }

    public void Sel2Players()
    {
        gameEngine.InitGame(2);
        foreach (Transform child in transform)
        {
            if(child.name == "Players")
            {
                child.gameObject.SetActive(true);
            }
        }
    }

    public void Sel3Players()
    {
        gameEngine.InitGame(3);
        foreach (Transform child in transform)
        {
            if (child.name == "Players")
            {
                child.gameObject.SetActive(true);
            }
        }
    }

    public void Sel4Players()
    {
        gameEngine.InitGame(4);
        foreach (Transform child in transform)
        {
            if (child.name == "Players")
            {
                child.gameObject.SetActive(true);
            }
        }
    }

}
