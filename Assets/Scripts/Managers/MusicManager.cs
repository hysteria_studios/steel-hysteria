﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour {

    //[Header("Instance")]
    public static MusicManager instance;

    //[Header("Sounds")]
    public AudioClip[] music;
    private AudioSource source;

    //[Header("Other")]
    public Scrollbar scrollbar;

    private void Awake()
    {
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }

    // Use this for initialization
    void Start() {
        source = transform.GetComponent<AudioSource>();
        scrollbar.value = source.volume;
        music = Resources.LoadAll<AudioClip>("Sounds/Music");
    }

    public AudioClip[] GetMusic()
    {
        return music;
    }

    public AudioSource GetMusicSource()
    {
        return source;
    }

    public Scrollbar GetMusicScrollbar()
    {
        return scrollbar;
    }

    public void ChangeMusicVolume()
    {
        source.volume = scrollbar.value;
    }
}
