using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets._2D
{
    public class RestarterScript : MonoBehaviour
    {

        void Update()
        {
            bool restart = InputManager.Instance.RestartButton();

            if(restart)
            {
                SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
            }
        }
    }
}
