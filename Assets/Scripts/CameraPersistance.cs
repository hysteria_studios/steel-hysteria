﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPersistance : MonoBehaviour {

    public static CameraPersistance instance;

	// Use this for initialization
	void Start () {
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }
}
