﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {

    private Text text;
    private GameManager gameManager;

    void Awake()
    {
        text = transform.Find("CountDown").gameObject.GetComponent<Text>();
    }

    public IEnumerator StartCountDown(GameManager gm)
    {
        gameManager = gm;

        text.text = "3";
        yield return new WaitForSeconds(1f);

        text.text = "2";
        yield return new WaitForSeconds(1f);

        text.text = "1";
        yield return new WaitForSeconds(1f);

        text.text = "Fight!";
        yield return new WaitForSeconds(1f);

        gameManager.isStarting = false;

    }
}
