﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    //[Header("Transform")]
    public Transform pauseMenu;

    //[Header("Boolean")]
    private bool pause = false;

    private void Start()
    {
        pauseMenu = transform.parent.GetChild(3);
    }

    // Update is called once per frame
    void Update () {
        pause = InputManager.Instance.PauseButton();
        if (pause)
        {
            if (!pauseMenu.gameObject.activeSelf)
            {
                Time.timeScale = 0;
                pauseMenu.gameObject.SetActive(true);              
            }
            else
            {
                Time.timeScale = 1;
                pauseMenu.gameObject.SetActive(false);
            }
        }
	}
}
