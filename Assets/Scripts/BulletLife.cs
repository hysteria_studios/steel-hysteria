﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLife : MonoBehaviour {

    public float maxSpeed = 10f;
    private Rigidbody2D rb2d;
    private GameObject inactiveGuns;
    private Transform playerContainer;
    private GameObject playerRef;

    private void Awake()
    {
        SettingReferences();
    }


    void Start()
    {
        // Destroy the bullet after 1 seconds
        Destroy(transform.gameObject, 0.5f);
    }

    void FixedUpdate()
    {
        rb2d.AddForce(Vector3.right * Time.deltaTime * maxSpeed);
        //transform.Translate(Vector3.right * Time.deltaTime * maxSpeed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag == "Player")
        {
            foreach (Transform child in playerContainer.transform)
            {
                if (collision.gameObject == child.gameObject) {
                    child.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
                    child.gameObject.GetComponent<Character2D>().Death();
                    Destroy(this.gameObject);
                }
            }
        }
        else if (collision.gameObject.tag == "Trap")
        {
            Destroy(this.gameObject);
        }
    }

    private void SettingReferences()
    {
        playerContainer = GameObject.Find("Players").transform;
        rb2d = GetComponent<Rigidbody2D>();
        inactiveGuns = GameObject.Find("InGameGuns");

        if (transform.parent.name == "P_1")
        {
            playerRef = GameObject.Find("Player_1");
        }
        else if (transform.parent.name == "P_2")
        {
            playerRef = GameObject.Find("Player_2");
        }
        else if (transform.parent.name == "P_3")
        {
            playerRef = GameObject.Find("Player_3");
        }
        else if (transform.parent.name == "P_4")
        {
            playerRef = GameObject.Find("Player_4");
        }


        foreach (Transform child in inactiveGuns.transform)
        {
            Physics2D.IgnoreCollision(child.gameObject.GetComponent<Collider2D>(), this.gameObject.GetComponent<Collider2D>(), true);
        }

        Physics2D.IgnoreCollision(transform.gameObject.GetComponent<Collider2D>(), playerRef.GetComponent<Collider2D>(), true);

        if (transform.rotation.x == 1f)
        {
            maxSpeed = -maxSpeed;
            gameObject.transform.localScale *= -1;
        }
    }
}
