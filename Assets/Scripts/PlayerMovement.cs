﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    //[Header("Boolean")]
    private bool jump;
    

    //[Header("Float")]
    private float h;
    private float v;

    //[Header("Game Manager")]
    private GameManager gameManager;

    //[Header("Character")]
    private Character2D character;


    private void Awake()
    {
        SettingReferences();
    }


    private void FixedUpdate()
    {
        if (!gameManager.isStarting && !gameManager.roundFinished)
        {
            if (!character.deathMasterEnabled)
            {
                jump = InputManager.Instance.JumpButton(character.player);
                h = InputManager.Instance.GetAxisHorizontal(character.player);
                if (Mathf.Abs(h) > 0.0001f || jump)
                {
                    character.Move(h, jump);
                }
                character.anim.SetFloat("Speed", Mathf.Abs(h));
            }
            else
            {
                if (!character.isPossessing)
                {
                    h = InputManager.Instance.GetAxisHorizontal(character.player);
                    v = InputManager.Instance.GetAxisVertical(character.player);

                    character.DeathMasterMove(h, v);
                }
            }
        }
    }

    private void SettingReferences()
    {
        character = GetComponent<Character2D>();
        gameManager = GameObject.Find("/GameManager").GetComponent<GameManager>();
    }    

}
