﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeMap : MonoBehaviour {

    public int totalMaps = 1;
    private string mapName = "Level_";

    public void nextMap() {
        int num = Random.Range(1, totalMaps);
        SceneManager.LoadScene(mapName + num.ToString());
    }
}
