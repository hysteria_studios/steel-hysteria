﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager> {

    public enum Player
    {
        PLAYER_1,
        PLAYER_2,
        PLAYER_3,
        PLAYER_4
    }

    public float GetAxisHorizontal(Player player)
    {
        switch (player)
        {
            case Player.PLAYER_1:
                return Input.GetAxis("Horizontal_P1");
            case Player.PLAYER_2:
                return Input.GetAxis("Horizontal_P2");
            case Player.PLAYER_3:
                return Input.GetAxis("Horizontal_P3");
            case Player.PLAYER_4:
                return Input.GetAxis("Horizontal_P4");
            default:
                return 0;
        }
    }

    public float GetAxisVertical(Player player)
    {
        switch (player)
        {
            case Player.PLAYER_1:
                return Input.GetAxis("Vertical_P1");
            case Player.PLAYER_2:
                return Input.GetAxis("Vertical_P2");
            case Player.PLAYER_3:
                return Input.GetAxis("Vertical_P3");
            case Player.PLAYER_4:
                return Input.GetAxis("Vertical_P4");
            default:
                return 0;
        }
    }

    public bool RestartButton()
    {
        return Input.GetButton("Restart");
    }

    public bool JumpButton(Player player)
    {
        switch (player)
        {
            case Player.PLAYER_1:
                return Input.GetButtonDown("Jump_P1");
            case Player.PLAYER_2:
                return Input.GetButtonDown("Jump_P2");
            case Player.PLAYER_3:
                return Input.GetButtonDown("Jump_P3");
            case Player.PLAYER_4:
                return Input.GetButtonDown("Jump_P4");
            default:
                return false;
        }
    }

    public bool FireButton(Player player, string armTag)
    {
        bool fire = false;

        if (armTag == "Pistol")
        {
            switch (player)
            {
                case Player.PLAYER_1:
                    fire = Input.GetButtonDown("Fire_P1");
                    break;
                case Player.PLAYER_2:
                    fire = Input.GetButtonDown("Fire_P2");
                    break;
                case Player.PLAYER_3:
                    fire = Input.GetButtonDown("Fire_P3");
                    break;
                case Player.PLAYER_4:
                    fire = Input.GetButtonDown("Fire_P4");
                    break;
                default:
                    break;
            }
        }
        else if (armTag == "Shotgun")
        {
            switch (player)
            {
                case Player.PLAYER_1:
                    fire = Input.GetButtonDown("Fire_P1");
                    break;
                case Player.PLAYER_2:
                    fire = Input.GetButtonDown("Fire_P2");
                    break;
                case Player.PLAYER_3:
                    fire = Input.GetButtonDown("Fire_P3");
                    break;
                case Player.PLAYER_4:
                    fire = Input.GetButtonDown("Fire_P4");
                    break;
                default:
                    break;
            }
        }
        else if (armTag == "Machinegun")
        {
            switch (player)
            {
                case Player.PLAYER_1:
                    fire = Input.GetButton("Fire_P1");
                    break;
                case Player.PLAYER_2:
                    fire = Input.GetButton("Fire_P2");
                    break;
                case Player.PLAYER_3:
                    fire = Input.GetButton("Fire_P3");
                    break;
                case Player.PLAYER_4:
                    fire = Input.GetButton("Fire_P4");
                    break;
                default:
                    break;
            }
        }
        return fire;
    }

    public bool PickButton(Player player)
    {
        switch (player)
        {
            case Player.PLAYER_1:
                return Input.GetButtonDown("Pick_P1");
            case Player.PLAYER_2:
                return Input.GetButtonDown("Pick_P2");
            case Player.PLAYER_3:
                return Input.GetButtonDown("Pick_P3");
            case Player.PLAYER_4:
                return Input.GetButtonDown("Pick_P4");
            default:
                return false;
        }
            
    }

    public bool TrapFireButton(Player player)
    {
        switch (player)
        {
            case Player.PLAYER_1:
                return Input.GetButtonDown("TrapFire_P1");
            case Player.PLAYER_2:
                return Input.GetButtonDown("TrapFire_P2");
            case Player.PLAYER_3:
                return Input.GetButtonDown("TrapFire_P3");
            case Player.PLAYER_4:
                return Input.GetButtonDown("TrapFire_P4");
            default:
                return false;
        }

    }

    public bool PossesButton(Player player)
    {
        switch (player)
        {
            case Player.PLAYER_1:
                return Input.GetButtonDown("Possess_P1");
            case Player.PLAYER_2:
                return Input.GetButtonDown("Possess_P2");
            case Player.PLAYER_3:
                return Input.GetButtonDown("Possess_P3");
            case Player.PLAYER_4:
                return Input.GetButtonDown("Possess_P4");
            default:
                return false;
        }

    }

    public bool BackButton()
    {
        return Input.GetButtonDown("Cancel");
    }

    public bool PauseButton()
    {
        return Input.GetButtonDown("Pause");
    }

    public bool ContinueButton()
    {
        return Input.GetButton("Jump_P1") || Input.GetButton("Jump_P2") || Input.GetButton("Jump_P3") || Input.GetButton("Jump_P1");
    }
}
