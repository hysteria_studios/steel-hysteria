﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IGame : MonoBehaviour {
    public enum GameResult {
        WIN_PLAYER_1,
        WIN_PLAYER_2,
        WIN_PLAYER_3,
        WIN_PLAYER_4
    }

    public enum DeathMaster {
        PLAYER_1,
        PLAYER_2,
        PLAYER_3,
        PLAYER_4
    }

    public abstract void InitGame(int numPlayers);

    public abstract void BeginGame();


}