﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShotAction : MonoBehaviour {

    //[Header("Character 2D")]
    private Character2D character;

    //[Header("Transform")]
    private Transform gunBulletSpawn;
    private Transform pistolBulletSpawn;
    private Transform machinegunBulletSpawn;
    private Transform bulletParent;
    private Transform gunsContainer;

    //[Header("Managers")]
    private SoundManager soundManager;

    //[Header("Game Object")]
    private GameObject bullet;

    //[Header("String")]
    private string armTag;

    //[Header("Quaternion")]
    private Quaternion bulletRotation = new Quaternion(0, 0, 0, 0);

    //[Header("Boolean")]
    private bool armEnabled = false;
    private bool isRunning = false;

    void Awake()
    {
        character = GetComponent<Character2D>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        SettingReferences();
    }

    private void FixedUpdate()
    {
        if (!character.deathMasterEnabled)
        {
            armEnabled = false;
            foreach (Transform child in gunsContainer)
            {
                if (child.gameObject.activeSelf)
                {
                    armEnabled = true;
                    armTag = child.gameObject.tag;
                }
            }

            bool shot = InputManager.Instance.FireButton(character.player, armTag);
            if (shot && armEnabled)
            {
                Shot();
            }
        }
    }


    public void Shot()
    {
        foreach (Transform child in gunsContainer)
        {
            if (child.gameObject.activeSelf)
            {
                if (gameObject.transform.localScale.x < 0)
                {
                    bulletRotation = new Quaternion(180, 0, 0, 0);
                }
                else if (gameObject.transform.localScale.x > 0)
                {
                    bulletRotation = new Quaternion(0, 0, 0, 0);
                }
                if (child.gameObject.tag == "Shotgun" && !isRunning)
                {
                    soundManager.GetSoundEffectsSource().PlayOneShot(soundManager.GetSoundEffects("Shotgun"), 0.5f);
                    Instantiate(bullet, gunBulletSpawn.transform.position + new Vector3(0.1f,0,0), bulletRotation, bulletParent);
                    StartCoroutine(ShotCoolDown());
                }
                else if (child.gameObject.tag == "Pistol" && !isRunning)
                {
                    soundManager.GetSoundEffectsSource().PlayOneShot(soundManager.GetSoundEffects("Pistol"), 0.5f);
                    Instantiate(bullet, pistolBulletSpawn.transform.position + new Vector3(0.1f, 0, 0), bulletRotation, bulletParent);
                    StartCoroutine(ShotCoolDown());
                }
                else if (child.gameObject.tag == "Machinegun" && !isRunning)
                {
                    soundManager.GetSoundEffectsSource().PlayOneShot(soundManager.GetSoundEffects("Machinegun"), 0.5f);
                    Instantiate(bullet, machinegunBulletSpawn.transform.position + new Vector3(0.1f, 0, 0), bulletRotation, bulletParent);
                    StartCoroutine(ShotCoolDown());
                }
            }
        }        
    }

    IEnumerator ShotCoolDown()
    {
        isRunning = true;
        if (armTag == "Shotgun")
        {
            yield return new WaitForSeconds(1f);
        }
        else if (armTag == "Pistol")
        {
            yield return new WaitForSeconds(0.5f);
        }
        else
        {
            yield return new WaitForSeconds(0.15f);
        }

        isRunning = false;
        
    }

    private void SettingReferences()
    {
        gunBulletSpawn = transform.Find("GunBulletSpawn");
        pistolBulletSpawn = transform.Find("PistolBulletSpawn");
        machinegunBulletSpawn = transform.Find("MachinegunBulletSpawn");
        if (transform.name == "Player_1")
        {
            bulletParent = GameObject.Find("/SceneGameObjects/BulletsParent/P_1").transform;
        }
        else if (transform.name == "Player_2")
        {
            bulletParent = GameObject.Find("/SceneGameObjects/BulletsParent/P_2").transform;
        }
        else if (transform.name == "Player_3")
        {
            bulletParent = GameObject.Find("/SceneGameObjects/BulletsParent/P_3").transform;
        }
        else if (transform.name == "Player_4")
        {
            bulletParent = GameObject.Find("/SceneGameObjects/BulletsParent/P_4").transform;
        }
        
        
        gunsContainer = transform.Find("GunsContainer");
        bullet = (GameObject)Resources.Load("Prefabs/Bullet");
    }
}
