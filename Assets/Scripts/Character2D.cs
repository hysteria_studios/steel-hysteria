﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character2D : MonoBehaviour {
    
    //[Header("Floats")]
    private float maxSpeed;
    private float jumpForce;
    const float groundedRadius = .2f;

    //[Header("Booleans")]
    private bool grounded;
    private bool facingRight;
    private bool possess;
    public bool deathMasterEnabled;
    public bool isPossessing;
    private bool wallCollision;
    private bool gunUp;
    private bool canMove;
    private bool isExploding;

    //[Header("Transform")]
    private Transform _transform;
    private Transform groundCheck;
    private Transform inactiveGuns;
    private Transform gunsContainer;
    private Transform playerInterface;
    private Transform indicator;
    private Transform scoreTable;

    //[Header("External Scripts")]
    private ArmPick pickScript;
    private SawTrap toPossesSaw;
    private LaserTrap toPossesLaser;
    private BoltTrap toPossesBolt;
    private GravityTrap toPossesGravity;

    //[Header("Managers")]
    private GameEngine gameEngine;
    private GameManager gameManager;
    private MenuManager menuManager;
    private SoundManager soundManager;

    //[Header("Animator")]
    public Animator anim;

    //[Header("Rigidbody")]
    private Rigidbody2D rdbd2D;
    
    //[Header("Player")]
    public InputManager.Player player;

    //[Header("Sprite")]
    public Sprite[] interfaceSprite;

    // Use this for initialization
    void Awake()
    {
        SettingReferences();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!gameManager.isStarting && !gameManager.roundFinished)
        {
            if (!deathMasterEnabled)
            {
                grounded = false;

                Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject && colliders[i].gameObject.name != "InterfaceCollision" && colliders[i].gameObject.tag != "Player" && colliders[i].gameObject.tag != "Machinegun" && colliders[i].gameObject.tag != "Shotgun" && colliders[i].gameObject.tag != "Pistol")
                    {
                        grounded = true;
                    }
                }

                anim.SetBool("Ground", grounded);
                anim.SetFloat("vSpeed", rdbd2D.velocity.y);
            }
            else
            {
                possess = InputManager.Instance.PossesButton(player);
                if (possess && !isPossessing)
                {
                    Posses();
                }
            }
        }
        else
        {
            rdbd2D.velocity = Vector2.zero;
        }
    }

    public void Move(float move, bool jump)
    {
        if (grounded && !isExploding)
        {
            anim.SetFloat("Speed", Mathf.Abs(move) * 1.1f);
            rdbd2D.velocity = new Vector2(move * maxSpeed * Time.deltaTime, rdbd2D.velocity.y);
            if (gunUp && canMove)
            {
                gunsContainer.position = new Vector2(gunsContainer.position.x, gunsContainer.position.y + 0.05f);
                canMove = false;
                StartCoroutine(GunPosition());
            }
            else if(!gunUp && canMove)
            {
                gunsContainer.position = new Vector2(gunsContainer.position.x, gunsContainer.position.y - 0.05f);
                canMove = false;
                StartCoroutine(GunPosition());
            }

            if (move > 0 && !facingRight)
            {
                Flip();
            }
            else if (move < 0 && facingRight)
            {
                Flip();
            }
        }
        else if(!grounded && !isExploding)
        {
            if (!wallCollision)
            {
                anim.SetFloat("Speed", Mathf.Abs(move));
                rdbd2D.velocity = new Vector2(move * maxSpeed * Time.deltaTime, rdbd2D.velocity.y);

                if (move > 0 && !facingRight)
                {
                    Flip();
                }
                else if (move < 0 && facingRight)
                {
                    Flip();
                }
            }
        }

        if (grounded && jump && anim.GetBool("Ground"))
        {
            grounded = false;
            anim.SetBool("Ground", false);
            rdbd2D.AddForce(new Vector2(0f, jumpForce * Time.deltaTime));
        }
    }

    IEnumerator GunPosition()
    {
        yield return new WaitForSeconds(0.1f);
        canMove = true;
        gunUp = !gunUp;
    }

    public void DeathMasterMove(float hMove, float vMove)
    {
        if (!isExploding)
        {
            rdbd2D.velocity = new Vector2(hMove * maxSpeed * Time.deltaTime, vMove * maxSpeed * Time.deltaTime * -1);
        }
    }

    private void Flip()
    {
        facingRight = !facingRight;
        transform.localScale = Vector3.Scale(transform.localScale, new Vector3(-1, 1, 1));
        Transform child = transform.Find("Indicator");
        child.localScale = Vector3.Scale(child.localScale, new Vector3(-1, 1, 1));
    }

    public void Death()
    {
        Vector3 prevPos = transform.position;
        transform.position = prevPos;
        foreach (Transform child in gunsContainer)
        {
            if (child.gameObject.activeSelf)
            {
                child.gameObject.SetActive(false);
            }
        }
        playerInterface.GetComponent<SpriteRenderer>().sprite = interfaceSprite[1];
        pickScript.isPicked = false;
        isExploding = true;
        transform.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        transform.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        transform.gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
        transform.gameObject.GetComponent<CircleCollider2D>().enabled = true;
        anim.SetBool("Death", true);
        deathMasterEnabled = true;
        gameManager.PlayerDeath(player);
        soundManager.GetSoundEffectsSource().PlayOneShot(soundManager.GetSoundEffects("Explosion"), 0.5f);
        StartCoroutine(Explosion());
    }

    IEnumerator Explosion() {
        yield return new WaitForSeconds(0.5f);
        transform.GetComponent<SpriteRenderer>().color = Color.white;
        yield return new WaitForSeconds(1.25f);
        anim.SetBool("Death", false);
        anim.SetBool("DeathMaster", deathMasterEnabled);
        isExploding = false;
        SetColor();
    }

    private void Posses()
    {
        isPossessing = true;

        if (toPossesSaw != null)
        {
            _transform.GetComponent<SpriteRenderer>().enabled = false;
            indicator.GetComponent<SpriteRenderer>().enabled = false;
            toPossesSaw.GetPossesed(_transform);
        }
        else if (toPossesBolt != null)
        {
            _transform.GetComponent<SpriteRenderer>().enabled = false;
            indicator.GetComponent<SpriteRenderer>().enabled = false;
            toPossesBolt.GetPossesed(_transform);
        }
        else if (toPossesLaser != null)
        {
            _transform.GetComponent<SpriteRenderer>().enabled = false;
            indicator.GetComponent<SpriteRenderer>().enabled = false;
            toPossesLaser.GetPossesed(_transform);
        }
        else if (toPossesGravity != null)
        {
            _transform.GetComponent<SpriteRenderer>().enabled = false;
            indicator.GetComponent<SpriteRenderer>().enabled = false;
            toPossesGravity.GetPossesed(_transform);
        }
        else
        {
            isPossessing = false;
        }
        
    }

    public void Desposses()
    {
        isPossessing = false;
        _transform.GetComponent<SpriteRenderer>().enabled = true;
        indicator.GetComponent<SpriteRenderer>().enabled = true;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Wall")
        {
            wallCollision = true;
        }
        else if (coll.gameObject.tag == "KillZone")
        {
            Death();
        }
        else if (coll.gameObject.tag == "Trap")
        {
            Death();
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Wall")
        {
            wallCollision = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (deathMasterEnabled) {
            if (other.name == "Saw")
            {
                toPossesSaw = other.GetComponent<SawTrap>();
                possess = true;
            }
            else if (other.name == "BoltTrap")
            {
                toPossesBolt = other.GetComponent<BoltTrap>();
                possess = true;
            }
            else if (other.name == "LaserTrap")
            {
                toPossesLaser = other.GetComponent<LaserTrap>();
                possess = true;
            } else if (other.name == "GravityTrap")
            {
                toPossesGravity = other.GetComponent<GravityTrap>();
                possess = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (deathMasterEnabled)
        {
            if (collision.name == "Saw")
            {
                toPossesSaw = null;
                possess = false;
            }
            else if (collision.name == "BoltTrap")
            {
                toPossesBolt = null;
                possess = true;
            }
            else if (collision.name == "LaserTrap")
            {
                toPossesLaser = null;
                possess = true;
            }
        }
    }

    private void SettingReferences()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameEngine = GameObject.Find("GameEngine").GetComponent<GameEngine>();
        menuManager = GameObject.Find("MenuManager").GetComponent<MenuManager>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        scoreTable = gameManager.scoreTable;
        maxSpeed = 250f;
        jumpForce = 28000f;
        possess = false;
        facingRight = true;
        deathMasterEnabled = false;
        isPossessing = false;
        wallCollision = false;
        gunUp = true;
        canMove = true;
        isExploding = false;
        groundCheck = transform.Find("GroundCheck");
        anim = GetComponent<Animator>();
        rdbd2D = GetComponent<Rigidbody2D>();
        pickScript = GetComponent<ArmPick>();
        _transform = GetComponent<Transform>();
        inactiveGuns = GameObject.Find("/SceneGameObjects/InGameGuns").transform;
        gunsContainer = transform.Find("GunsContainer");
        indicator = transform.Find("Indicator");
        toPossesSaw = null;
        toPossesBolt = null;
        toPossesLaser = null;
        toPossesGravity = null;
        interfaceSprite = Resources.LoadAll<Sprite>("Sprites/Canvas/CharacterInterface");
        SetPlayer();

        foreach (Transform child in inactiveGuns)
        {
            Physics2D.IgnoreCollision(child.gameObject.GetComponent<Collider2D>(), transform.gameObject.GetComponent<Collider2D>(), true);
            foreach (Transform gunsChild in inactiveGuns)
            {
                Physics2D.IgnoreCollision(child.gameObject.GetComponent<Collider2D>(), gunsChild.gameObject.GetComponent<Collider2D>(), true);
            }
        }

        foreach (Transform child in transform.parent)
        {
            if (child != transform)
            {
                Physics2D.IgnoreCollision(child.gameObject.GetComponent<Collider2D>(), transform.gameObject.GetComponent<Collider2D>(), true);
            }

        }
    }

    private void SetPlayer()
    {
        SetName();
        if (gameObject.name == "Player_1")
        {
            player = InputManager.Player.PLAYER_1;
            playerInterface = menuManager.transform.Find("Interface/Player_1");
            playerInterface.GetComponent<SpriteRenderer>().sprite = interfaceSprite[0];
            indicator.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animations/Robot/Indicators/IndicatorContP_1");
            scoreTable.GetChild(2).gameObject.SetActive(true);
        }
        else if (gameObject.name == "Player_2")
        {
            player = InputManager.Player.PLAYER_2;
            playerInterface = menuManager.transform.Find("Interface/Player_2");
            playerInterface.GetComponent<SpriteRenderer>().sprite = interfaceSprite[0];
            indicator.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animations/Robot/Indicators/IndicatorContP_2");
            scoreTable.GetChild(3).gameObject.SetActive(true);
        }
        else if (gameObject.name == "Player_3")
        {
            player = InputManager.Player.PLAYER_3;
            playerInterface = menuManager.transform.Find("Interface/Player_3");
            playerInterface.GetComponent<SpriteRenderer>().sprite = interfaceSprite[0];
            indicator.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animations/Robot/Indicators/IndicatorContP_3");
            scoreTable.GetChild(4).gameObject.SetActive(true);
        }
        else if (gameObject.name == "Player_4")
        {
            player = InputManager.Player.PLAYER_4;
            playerInterface = menuManager.transform.Find("Interface/Player_4");
            playerInterface.GetComponent<SpriteRenderer>().sprite = interfaceSprite[0];
            indicator.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animations/Robot/Indicators/IndicatorContP_4");
            scoreTable.GetChild(5).gameObject.SetActive(true);
        }
        SetColor();
    }

    private void SetName()
    {
        if (gameObject.name == "Player_1(Clone)")
        {
            gameObject.name = "Player_1";
        }
        else if (gameObject.name == "Player_2(Clone)")
        {
            gameObject.name = "Player_2";
        }
        else if (gameObject.name == "Player_3(Clone)")
        {
            gameObject.name = "Player_3";
        }
        else if (gameObject.name == "Player_4(Clone)")
        {
            gameObject.name = "Player_4";
        }
    }

    private void SetColor()
    {
        if (gameObject.name == "Player_1")
        {
            transform.GetComponent<SpriteRenderer>().color = Color.red;
            indicator.GetComponent<SpriteRenderer>().color = Color.red;
            playerInterface.GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (gameObject.name == "Player_2")
        {
            transform.GetComponent<SpriteRenderer>().color = Color.blue;
            indicator.GetComponent<SpriteRenderer>().color = Color.blue;
            playerInterface.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        else if (gameObject.name == "Player_3")
        {
            transform.GetComponent<SpriteRenderer>().color = Color.green;
            indicator.GetComponent<SpriteRenderer>().color = Color.green;
            playerInterface.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else if (gameObject.name == "Player_4")
        {
            transform.GetComponent<SpriteRenderer>().color = Color.yellow;
            indicator.GetComponent<SpriteRenderer>().color = Color.yellow;
            playerInterface.GetComponent<SpriteRenderer>().color = Color.yellow;
        }
    }

    public void ScoresTableText()
    {
        if (gameObject.name == "Player_1")
        {
            scoreTable.GetChild(2).GetComponent<Text>().text = "PLAYER 1  .................................................  " + gameEngine.scoreTable[player].ToString();
        }
        else if (gameObject.name == "Player_2")
        {
            scoreTable.GetChild(3).GetComponent<Text>().text = "PLAYER 2  .................................................  " + gameEngine.scoreTable[player].ToString();
        }
        else if (gameObject.name == "Player_3")
        {
            scoreTable.GetChild(4).GetComponent<Text>().text = "PLAYER 3  .................................................  " + gameEngine.scoreTable[player].ToString();
        }
        else if (gameObject.name == "Player_4")
        {
            scoreTable.GetChild(5).GetComponent<Text>().text = "PLAYER 4  .................................................  " + gameEngine.scoreTable[player].ToString();
        }
    }
}
