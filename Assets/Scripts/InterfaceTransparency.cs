﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceTransparency : MonoBehaviour {

    //[Header("Transform")]
    private Transform interfaceContainer;

	// Use this for initialization
	void Start () {
        interfaceContainer = transform.parent;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            foreach (Transform child in interfaceContainer)
            {
                if (child != transform)
                {
                    child.GetComponent<SpriteRenderer>().color = new Color(child.GetComponent<SpriteRenderer>().color.r, child.GetComponent<SpriteRenderer>().color.g, child.GetComponent<SpriteRenderer>().color.b, 0.3f);
                }
            }
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            foreach (Transform child in interfaceContainer)
            {
                if (child != transform)
                {
                    child.GetComponent<SpriteRenderer>().color = new Color(child.GetComponent<SpriteRenderer>().color.r, child.GetComponent<SpriteRenderer>().color.g, child.GetComponent<SpriteRenderer>().color.b, 0.3f);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            foreach (Transform child in interfaceContainer)
            {
                if (child != transform)
                {
                    child.GetComponent<SpriteRenderer>().color = new Color(child.GetComponent<SpriteRenderer>().color.r, child.GetComponent<SpriteRenderer>().color.g, child.GetComponent<SpriteRenderer>().color.b, 1f);
                }
            }
        }
    }
}
