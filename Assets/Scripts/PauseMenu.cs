﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public Transform optionsObj;
    public Transform _transformChild;
    public GameEngine gameEngine;

    private void Awake()
    {
        optionsObj.gameObject.SetActive(false);
        transform.gameObject.SetActive(false);
    }

    public void ResumeButton()
    {
        Time.timeScale = 1;
        if (optionsObj.gameObject.activeSelf)
        {
            optionsObj.gameObject.SetActive(false);
        }
        transform.gameObject.SetActive(false);
    }

    public void OptionsButton()
    {
        _transformChild.gameObject.SetActive(false);
        optionsObj.gameObject.SetActive(true);
    }

    public void MenuButton()
    {
        Time.timeScale = 1;
        gameEngine.Reset();
        SceneManager.LoadScene("Menu");
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
