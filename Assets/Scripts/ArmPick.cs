﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmPick : MonoBehaviour {

    //[Header ("Character")]
    private Character2D character;

    //[Header ("Transform")]
    public Transform gunsContainer;
	public Transform gunsInactive;

    //[Header ("Game Object")]
    public GameObject gunToPick;
    public GameObject pickedGun;

    //[Header ("Boolean")]
    public bool isPicked;
    private bool canBePicked;

    //[Header ("String")]
    private string gunName = "";

    private void Awake()
    {
        SettingReferences();
    }

    // Update is called once per frame
    void Update ()
    {
        if (!character.deathMasterEnabled)
        {
            bool isPicking = InputManager.Instance.PickButton(character.player);

            if (isPicking && !isPicked && canBePicked)
            {
                PickWeapon();
            }
            else if (!canBePicked && isPicking && isPicked)
            {
                DropWeapon();
            }
        }
    }

	private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isPicked)
        {
            if (collision.gameObject.tag == "Shotgun" || collision.gameObject.tag == "Pistol" || collision.gameObject.tag == "Machinegun")
            {
                canBePicked = true;
                gunName = collision.gameObject.name;
                gunToPick = collision.gameObject;
            }
        }
	}

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Shotgun" || collision.gameObject.tag == "Pistol" || collision.gameObject.tag == "Machinegun")
        {
            canBePicked = false;
            if (isPicked)
            {
                gunName = "";
                gunToPick = null;
            }
            
        }
    }

    private void PickWeapon()
    {
        pickedGun = gunToPick;
        foreach (Transform child in gunsContainer)
        {
            if (child.gameObject.name == gunName)
            {

                child.gameObject.SetActive(true);
                pickedGun.transform.position = gunsInactive.position;
            }
        }
        isPicked = true;
    }

    private void DropWeapon()
    {
        foreach (Transform child in gunsContainer)
        {
            if (child.gameObject.activeSelf)
            {
                child.gameObject.SetActive(false);
                pickedGun.transform.position = transform.position;
            }
        }
        isPicked = false;
    }

    private void SettingReferences()
    {
        isPicked = false;
        canBePicked = false;
        gunToPick = GetComponent<GameObject>();
        pickedGun = GetComponent<GameObject>();
        character = GetComponent<Character2D>();
        gunToPick = null;

        gunsContainer = transform.Find("GunsContainer");
        gunsInactive = GameObject.Find("InGameGuns").transform;
    }
}
